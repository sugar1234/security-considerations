## Problem #1
* The app doesn’t verify that the apk is signed by the official developers key. 

### Why?
* This can lead to anyone rewriting part of the code -like enabling the debug flag- and recompiling and the app will still behave like an official app. 

### Solution.
* Verify that the signature corresponds to the official developers key, as early as possible in the execution flow. Here is an [example](https://www.airpair.com/android/posts/adding-tampering-detection-to-your-android-app).

---
---

## Problem #2
* There is no memory encryption for some important parts of the code like coins/stunts. 

### Why?
* With a run of the mill memory editor like [game guardian](https://gameguardian.en.uptodown.com/android), you can find the memory value of the above mentioned resources and change them at will. These will be then used to caclulate your score. 

### Solution.
* Store the display value in plain-text so it’s easy to find. Make them think memory editing doesn’t work. 
* Store the actual value with an encryption that can guarantee that there is no consistent >, =, < correlation between display value and actual value. 
* Use multiple memory locations to hold parts of a resource and do some computation on them to return the actual value. (too much?)

---
---

## Problem #3
* You get points just for getting in the game even before the first hurdle.

### Why?
* Very easy to automate. A single tap at the replay button every 10 seconds would do the job.

### Solution.
* Just start counting steps/points as soon as the first coin is picked up. This way it's a lot harder to automate since the bot would have to somehow recognize the random generated structures.

---
---

## Problem #4
* No emulation detection.

### Why?
* It's a whole lot easier to debug and reverse code if you can run the application from the comfort of your computer with a clean environment.

### Solution
* Add some detection as shown [here.](https://www.airpair.com/android/posts/adding-tampering-detection-to-your-android-app#4-1-emulator) -same link as above-

---
---

## Problem #5
* The server trusts the client blindly. There is no attempt to verify that the data sent by the client are plausible.

### Why?
* From a security perspective there is no such thing as a working DRM without server synchronization. It's the only viable option right now at least until encrypted instructions become a thing. But compromises can be made. After all to payout game credits to TTT you have to connect to the Internet. I propose a two-pronged approach to allow for offline gaming over extended periods of time without compromising the economy completely.

### Solution
* While online:
    * The server HAS to synchronize the points before and after a level. HTTPS Pinning (HPKP) is required to prevent man in the middle attacks. [Medium Article](https://medium.com/@appmattus/android-security-ssl-pinning-1db8acb6621e)
    * A server based version of the below mentioned algorithm will be used.
   
* While offline:
    * A native C algorithm - [pseudocode](pseudocode.md) - that gets embedded to the android/ios app made by the TAP team and shared along with the current integration library. It would sit a top of any security the game already has.



    
