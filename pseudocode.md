# Pseudocode for client based DRM.

# Constraints to make this model work
* The time should be taken from hardware, after all we only need intervals and not the current time. Don't trust the local software clock.
* The server should check for the signature of the app and if the entity sending it data is in fact the app and not some reversed protocol thin client.
* There should be some every 20 hours played or 200.000 points earned -or something equivalent- you need to connect to the Internet, clause. The server would then be able to check if the amount of points earned are possible to be earned in the timeframe since the client last connected. 
* Payout should be always done through the app server and not directly from the client.
* This is essentially security through obscurity which is [not the best](https://en.wikipedia.org/wiki/Security_through_obscurity).



### Potential Attack Vectors:
* Total reversing of the native algorithm leads to complete control.. That can always happen if you have to trust the client. But it can be made really really hard. 
* Memory editing doesn’t work at all. The client will immediately realize the block isn’t valid and throw it out (and close the game?)
* Man in the middle attacks are useless because the data is encrypted along with the connection. 
        

Client Side.

    main();
        While True:
            // This will be called every 10 seconds with only the newly added information.
            Success = check_if_resources_are_valid(resources, game_objects, user_actions);
            If success:
                Block = write_out_current_calculations_to_a_block();
                Block = encrypt(block);
                blockchain.append(block);
            else 
                exit_app();
            sleep(10);

    on_server_connection();
        send_to_server(blockchain);
    
    check_if_resources_are_valid(resources, game_objects, user_actions);
        // resources for Stunt Runner would be stunts, coins, steps, points, gems
        for resource in resources:
            if resource.earned < 0 OR resource.earned > resource.max_possible_earned: 
                return false

        // This is to stop someone from changing the generation to only generate a continuous flat surface.
        for object in game_objects:
            if object.times_generated < 0 OR object.times_generated > object.max_possible:
                return false
        // This is to make botting harder. You could also check if the swipes are completely random since humans follow patterns but this would have to be done on the whole blockchain and not in each block.
        for user_action in user_actions:
            if is_direct_line(user_action):
                user.threshold += 1;
            if user.threshold > 10:
                return false

Server Side:

    on_data_received(blockchain):
        Dec_blockchain = decrypt_blockchain(blockchain);
        Success = check_for_validity_of_data(blockchain);
        If (success)
            connect_with_tap_platform_and_payout();