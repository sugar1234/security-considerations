## General recommendations for futute additions to the platform.

1. Use [ProGuard](https://www.guardsquare.com/en/products/proguard). 
    * It offers obfuscation of Java code, run time protection against tampering and debugging, faster execution times and smaller executable size. 
    * There is no real reason to use the paid upgrade. It will just take the same people that would crack ProGuard a little longer.
2. Don't use strings in the code. 
    * Strings offer direction and entry points to all the would be crackers. 
    * If you absolutely need to use them encrypt them separately. 
    * [Here](https://proandroiddev.com/secure-data-in-android-encryption-7eda33e68f58) is a series of articles on this subject.
3. Verify apk's signature and hash.
    * Before starting normal execution in the Android app.
    * Should be the first action when connecting to the server.

5. For extra peace of mind Google's [Safety Net](https://developer.android.com/training/safetynet/) is an option 
    * It comes with some non trivial drawbacks like the inability to serve users in China or rooted phones. - [In-depth Analysis](https://koz.io/inside-safetynet/) - 
6. Use emulation detection as shown [here.](https://www.airpair.com/android/posts/adding-tampering-detection-to-your-android-app#4-1-emulator) 
    * Combine different methods.
7. Separate the display values from the actual values used for computation. 
    * Let the display values unencrypted in memory. This makes them think the resources are constantly synced with the server.
    * Encrypt the actual values used for computation and make sure the encryption algorithm doesn't produce results that are <, =, > correlated with the unencrypted values. 
    * This applies to all resources like gold, gems, etc.
8. Make sure the user needs to apply some random non predictable action before starting to earn points in a level. 
    * For example in a endless runner game, the user would need to jump some random generated obstacles a couple of times before earning points.
    * This makes it harder to automate point generation. 
9. Track user movements. 
    * Flag direct lines when swiping or completely random swipes as bot behaviour. 
    * Ban after a threshold is passed.
10. Use HTTPS Pinning (HPKP) everywhere. 
    * This makes it harder to insert a [HTTPS decrypting proxy](https://www.telerik.com/fiddler) between the app and the server.
    * [Medium Article](https://medium.com/@appmattus/android-security-ssl-pinning-1db8acb6621e) on HPKP for Android.
11. Don't rely on the local software clock for any calculations!

## Recommendations that would make more sense to be implemented once in some common library.

1. Put some kind of limit on different accounts from the same IP.
    * Add whitelist / blacklist options.
2. Tie the accounts with a physical device.
    * Ask for two factor authentication when adding a new device.
    * Limit the number of devices.
3. Incase of multiple accounts per device.
    * Make some sanity checks that not all accounts earn the maximum amount possible simultaneously.
4. Use some kind of block based approach on tracking metrics.
    * For example record every 10 seconds the amount of gold earned. Is it between 0 and the max amount that could be gained legally?
    * [Here](pseudocode.md) is a rough draft on how that would look.
    * Handle this server side if the client is connected to the Internet else locally.
    * Make it a requirement for clients to connect every X hours played/ X resources earned to the server for a checkup. 
    * When a client connects have the server check on the plausability of the data reported. 
        * If the client connects every 3 days but reports earnings of 5 days playtime discard the request for payment.
        * Establish some kind of average earning / day from data. Manually investigate extreme outliers.
5. The Tap Platform should never directly communicate with the client. It should receive all the block data from the server, verify again and the payout. 
        